//
//  ViewController.swift
//  Hofin-CodeChallenge
//
//  Created by salmani on 10/1/22.
//

import UIKit

class ViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet var txtFirstString: UITextField!
    @IBOutlet var txtRotationString: UITextField!
    @IBOutlet var btnCheck: BadgeButton!
    
    private var numberOfTrueResults = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBadgeView()
    }
    
    //MARK: - IBActions
    @IBAction func onBtnCheckTapped(_ sender: BadgeButton) {
        
        if let firstString = txtFirstString.text, firstString.count != 0, let rotatedString = txtRotationString.text, rotatedString.count != 0 {
            let isRotated = isRotated(firstString: firstString, rotatedString: rotatedString)
            if isRotated { numberOfTrueResults += 1 }
        }
        
        sender.badge = "\(numberOfTrueResults)"
    }
    
    //MARK: - Functions
    private func isRotated(firstString: String, rotatedString: String) -> Bool {
        if firstString.count != rotatedString.count { return false }
        let combinedStrings = firstString.lowercased() + firstString.lowercased()
        
        return combinedStrings.contains(rotatedString.lowercased())
    }
    
    private func setupBadgeView() {
        btnCheck.badge = "\(numberOfTrueResults)"
    }
}
